# Define the Docker image name and tag
IMAGE_NAME := lovino-fe
IMAGE_TAG := latest

# Define the Docker command
DOCKER := docker

# Define the Docker Compose command
DOCKER_COMPOSE := docker-compose

# Define the build command
build:
	$(DOCKER_COMPOSE) build --no-cache

# Define the run command
run:
	$(DOCKER_COMPOSE) up

# Define the stop command
stop:
	$(DOCKER_COMPOSE) down

# Define the clean command
clean:
	$(DOCKER) image rm -f $(IMAGE_NAME):$(IMAGE_TAG)